# tgdown
Because I wanna download images from telegraph pages automaticly.

## Usage:

`tgdown` accepts two arguments:

* `URL` - Telegraph URL

* `PATH` - Save path. Defaults to current web page title. If path does not exist tries to create save directory recursively.

## Example:

`tgdown https://telegra.ph/My-first-Telegraph-story-11-23 story`
