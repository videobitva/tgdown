use anyhow::{Context, Result};
use tl::VDom;

use crate::error::ParseError;

pub(crate) fn dom<'a>(data: &'a str) -> Result<VDom<'a>> {
    Ok(tl::parse(data, tl::ParserOptions::default()).map_err(|_| ParseError::InitParsingFailed)?)
}

/// Parse HTML and retrieve image hrefs
pub(crate) fn retrieve_links<'a>(dom: &'a VDom<'a>) -> Result<Vec<String>> {
    let parser = dom.parser();

    let srcs = dom
        .query_selector("img")
        .ok_or(ParseError::ImageNotFound)?
        .filter_map(|handle| {
            handle.get(parser).and_then(|node| {
                node.as_tag().and_then(|tag| {
                    tag.attributes().get("src").and_then(|val| {
                        val.and_then(|val| {
                            std::str::from_utf8(val.as_bytes())
                                .map(|slice| slice.to_string())
                                .ok()
                        })
                    })
                })
            })
        })
        .collect::<Vec<String>>();

    Ok(srcs)
}

pub(crate) fn retrieve_title<'a>(dom: &'a VDom<'a>) -> Result<String> {
    let parser = dom.parser();

    let maybe_title = dom
        .query_selector("title")
        .ok_or(ParseError::ImageNotFound)?
        .filter_map(|handle| {
            handle.get(parser).and_then(|node| {
                node.as_tag()
                    .and_then(|tag| Some(tag.inner_text(parser).into_owned()))
            })
        })
        .nth(0);

    maybe_title.context(ParseError::TitleNotFound)
}
