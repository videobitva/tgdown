use std::path::PathBuf;

use crate::parse;
use anyhow::Result;

use futures::StreamExt;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};

use tokio::fs::File;
use tokio::io::AsyncWriteExt;

/// Load images from telegra.ph page and save them to path
pub(crate) async fn do_things(url: &str, path: Option<String>) -> Result<()> {
    let body = reqwest::get(url).await?.text().await?;
    let dom = parse::dom(&body)?;
    let title = parse::retrieve_title(&dom)?;
    let sources = parse::retrieve_links(&dom)?;

    let path = path.unwrap_or(title);

    let loader = Loader::new();
    loader.load(&sources, &path).await?;

    Ok(())
}

pub(crate) struct Loader {
    client: reqwest::Client,
    multi_progress: MultiProgress,
}

impl Loader {
    pub(crate) fn new() -> Self {
        let client = reqwest::Client::new();
        let multi_progress = MultiProgress::new();

        Self {
            client,
            multi_progress,
        }
    }

    pub(crate) async fn load(&self, urls: &[String], path: &str) -> Result<()> {
        std::fs::create_dir_all(path)?;

        let tasks = urls.iter().map(|url| {
            Self::load_task(
                self.client.clone(),
                self.multi_progress.clone(),
                url.to_string(),
                path.to_string(),
            )
        });

        futures::future::join_all(tasks).await;

        Ok(())
    }

    async fn load_task(
        client: reqwest::Client,
        queue: MultiProgress,
        url: String,
        path: String,
    ) -> Result<()> {
        let response = client.get(&url).send().await?;

        let total_bytes = response.content_length().unwrap();
        let mut downloaded_bytes = 0_u64;

        let pb = queue.add(ProgressBar::new(total_bytes));

        pb.set_position(0);
        pb.set_style(ProgressStyle::default_bar().template("{msg}\n{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({bytes_per_sec}, {eta})")?.progress_chars("#>-"));
        pb.set_message(format!("Downloading {}", url));

        let file_name = url.split("/").last().unwrap();
        let path = [path, file_name.to_owned()].iter().collect::<PathBuf>();

        let mut stream = response.bytes_stream();
        let mut file = File::create(path).await?;

        while let Some(Ok(bytes)) = stream.next().await {
            file.write_all(&bytes).await?;

            downloaded_bytes = (downloaded_bytes + bytes.len() as u64).min(total_bytes);

            pb.set_position(downloaded_bytes);
        }

        queue.remove(&pb);

        Ok(())
    }
}
