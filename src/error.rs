use thiserror::Error;

#[derive(Error, Debug)]
pub(crate) enum ParseError {
    #[error("Image tag not found")]
    ImageNotFound,
    #[error("Title not found")]
    TitleNotFound,
    #[error("Initial DOM parsing failed")]
    InitParsingFailed,
}
