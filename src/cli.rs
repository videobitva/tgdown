use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub(crate) struct Args {
    /// Telegra.ph link
    #[arg()]
    pub(crate) url: String,

    /// Images save path
    #[arg(short, long)]
    pub(crate) path: Option<String>,
}
