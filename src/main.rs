mod cli;
mod error;
mod load;
mod parse;

use anyhow::Result;
use clap::Parser;
use cli::Args;

#[tokio::main]
async fn main() -> Result<()> {
    match std::env::var("RUST_LOG") {
        Ok(log_level) => {
            log::info!("Log level set to `{log_level}`")
        }
        Err(_) => {
            log::warn!("Log level not set, setting log level to `error`");
            std::env::set_var("RUST_LOG", "error");
        }
    };

    env_logger::init().unwrap();

    let args = Args::parse();

    load::do_things(&args.url, args.path).await?;

    Ok(())
}
